#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <vector>
#include <algorithm>
#include <math.h>

long getTime          ();
long insertionSort    (std::vector<int>* A);
void insertionSort_t  ();
long heapSort         (std::vector<int>* A);
void maxHeapify       (std::vector<int>* A, int root, int heapSize);
void buildMaxHeap     (std::vector<int>* A);
void heapSort_t       ();
long mergeSort        (std::vector<int>* A, int initialIndex, int finalIndex);
void merge            (std::vector<int>* A, int initialIndex, int middleIndex, int finalIndex);
void mergeSort_t      ();

int main()
{
    srand(time(NULL));
    mergeSort_t();
    heapSort_t();
    insertionSort_t();
    return 0;
}

long insertionSort(std::vector<int> *A)
{
    long start = getTime();
    int j   = 0,
        val = 0;
    for(unsigned int i = 0; i < A->size(); ++i)
    {
        j = i;

        while(j > 0 && (A->at(j) < A->at(j-1)))
        {
            val        = A->at(j);
            A->at(j)   = A->at(j-1);
            A->at(j-1) = val;
            --j;
        }
    }
    return (getTime() - start);
}

void insertionSort_t()
{
    for(int l = 1; l <= 20; ++l)
    {
        //create ten test vectors
        std::vector< std::vector<int>* > vectorList;
        for(int i = 0; i < 10; ++i)
        {
            std::vector<int>* newVector = new std::vector<int>;
            vectorList.push_back(newVector);
        }

        //populate the vectors with test data
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            for(int j = 0; j < 1000 * l; ++j)
            {
                int randomNumber = rand() % 32767;
                vectorList[i]->push_back(randomNumber);

            }
        }

        //sort the arrays
        long time = 0; //stores the execution time of the algorithm
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
           time = time + insertionSort(vectorList[i]);
        }
        //output the average execution time over 10 trials for a given N
        std::cout << "Insertion sort execution time (s) for n = " << 1000 * l << ": " << (time / 10.0) / 1000000.0 << std::endl;
        //clean up the vectors for the next test iteration
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            delete vectorList[i];
        }
    }
}

long getTime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec* 1000000) + tv.tv_usec;
}

void maxHeapify(std::vector<int>* A, int root, int heapSize)
{
    int leftElement = root*2;
    int rightElement = root*2 + 1;
    int largest = -1;
    if(leftElement <= heapSize && A->at(leftElement) > A->at(root))
        largest = leftElement;
    else
        largest = root;
    if(rightElement <= heapSize && A->at(rightElement) > A->at(largest))
        largest = rightElement;
    if (largest != root)
    {
        std::swap(A->at(root), A->at(largest));
        maxHeapify(A, largest, heapSize);
    }
}

void buildMaxHeap(std::vector<int>* A)
{
    int heapSize = A->size() - 1;
    for(int i = floor(heapSize/2); i > 1; --i)
    {
        maxHeapify(A, i, heapSize);
    }
}

long heapSort(std::vector<int>* A)
{
    long start = getTime();
    int heapSize = A->size() - 1;
    buildMaxHeap(A);
    for(int i = heapSize; i > 0; --i)
    {
        std::swap(A->at(0), A->at(i));
        --heapSize;
        maxHeapify(A, 0, heapSize);
    }
        return (getTime() - start);
}

void heapSort_t()
{
    for(int l = 1; l <= 20; ++l)
    {
        //create ten test vectors
        std::vector< std::vector<int>* > vectorList;
        for(int i = 0; i < 10; ++i)
        {
            std::vector<int>* newVector = new std::vector<int>;
            vectorList.push_back(newVector);
        }

        //populate the vectors with test data
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            for(int j = 0; j < 1000 * l; ++j)
            {
                int randomNumber = rand() % 32767;
                vectorList[i]->push_back(randomNumber);

            }
        }

        //sort the arrays
        long time = 0; //stores the execution time of the algorithm
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
           time = time + heapSort(vectorList[i]);
        }
        //output the average execution time over 10 trials for a given N
        std::cout << "Heap sort execution time (s) for n = " << 1000 * l << ": " << (time / 10.0) / 1000000.0 << std::endl;
        //clean up the vectors for the next test iteration
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            delete vectorList[i];
        }
    }
}

long mergeSort(std::vector<int>* A, int initialIndex, int finalIndex)
{
    long start = getTime();
    if(initialIndex < finalIndex)
    {
        int middleIndex = floor((initialIndex + finalIndex) / 2)   ;
        mergeSort(A, initialIndex, middleIndex);
        mergeSort(A, middleIndex+1, finalIndex);
        merge(A, initialIndex, middleIndex, finalIndex);
        return (getTime() - start);
    }
    return -1;
}

void merge(std::vector<int>* A, int initialIndex, int middleIndex, int finalIndex)
{
    int i = initialIndex,
        k = initialIndex,
        j = middleIndex + 1;
    std::vector<int>* tempArray = new std::vector<int>;
    tempArray->resize(A->size());
    while(i <= middleIndex && j <= finalIndex)
    {
        if(A->at(i) < A->at(j))
        {
            tempArray->at(k) = A->at(i);
            ++k;
            ++i;
        }
        else
        {
            tempArray->at(k) = A->at(j);
            ++k;
            ++j;
        }
    }
    while(i <= middleIndex)
    {
        tempArray->at(k) = A->at(i);
        ++k;
        ++i;
    }
    while(j <= finalIndex)
    {
        tempArray->at(k) = A->at(j);
        ++k;
        ++j;
    }
    for(i = initialIndex; i < k; ++i)
        A->at(i) = tempArray->at(i);
    delete tempArray;
}


void mergeSort_t()
{
    for(int l = 1; l <= 20; ++l)
    {
        //create ten test vectors
        std::vector< std::vector<int>* > vectorList;
        for(int i = 0; i < 10; ++i)
        {
            std::vector<int>* newVector = new std::vector<int>;
            vectorList.push_back(newVector);
        }

        //populate the vectors with test data
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            for(int j = 0; j < 1000 * l; ++j)
            {
                int randomNumber = rand() % 32767;
                vectorList[i]->push_back(randomNumber);

            }
        }

        //sort the arrays
        long time = 0; //stores the execution time of the algorithm
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
           time = time + mergeSort(vectorList[i], 0, vectorList[i]->size() - 1);
        }
        //output the average execution time over 10 trials for a given N
        std::cout << "Merge sort execution time (s) for n = " << 1000 * l << ": " << (time / 10.0) / 1000000.0 << std::endl;
        //clean up the vectors for the next test iteration
        for(unsigned int i = 0; i < vectorList.size(); ++i)
        {
            delete vectorList[i];
        }
    }
}
